package io.naradrama.producer.aggregate.eventproxy.kafka;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface TestSource {
    //
    String TEST_SOURCE = "test-output";

    @Output(TEST_SOURCE)
    MessageChannel output();
}
