package io.naradrama.producer.aggregate.producer.store;

import io.naradrama.producer.aggregate.producer.entity.Producer;
import java.util.List;
import io.naradrama.shared.Offset;

public interface ProducerStore {
    void create(Producer producer);
    Producer retrieve(String id);
    List<Producer> retrieveAll(Offset offset);
    void update(Producer producer);
    void delete(Producer producer);
    boolean exists(String id);
    long countAll();
}
