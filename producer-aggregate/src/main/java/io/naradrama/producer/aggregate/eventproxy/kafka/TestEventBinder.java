package io.naradrama.producer.aggregate.eventproxy.kafka;

import io.naradrama.producer.aggregate.producer.entity.Producer;
import io.naradrama.producer.aggregate.producer.event.TestEvent;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@EnableBinding(TestSource.class)
public class TestEventBinder {
    //
    private TestSource testSource;

    public TestEventBinder(TestSource testSource) {
        this.testSource = testSource;
    }

    public void publishTestEvent(Producer producer) {
        //
        TestEvent event = new TestEvent(producer);
        testSource.output().send(MessageBuilder
                .withPayload(event)
                .setHeader("eventType", event.getClass().getSimpleName())
                .build());
    }
}
