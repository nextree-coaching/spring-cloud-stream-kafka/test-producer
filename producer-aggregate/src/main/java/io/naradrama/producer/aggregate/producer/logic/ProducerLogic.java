package io.naradrama.producer.aggregate.producer.logic;

import io.naradrama.shared.NameValueList;
import io.naradrama.shared.Offset;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import io.naradrama.producer.aggregate.producer.service.ProducerTaskService;
import io.naradrama.producer.aggregate.producer.store.ProducerStore;
import org.springframework.beans.factory.annotation.Autowired;
import io.naradrama.producer.facade.aggregate.producer.sdo.ProducerCdo;
import io.naradrama.producer.aggregate.producer.entity.Producer;
import java.util.NoSuchElementException;
import java.util.List;
import io.naradrama.shared.Offset;
import io.naradrama.shared.NameValueList;

@Service
@Transactional
public class ProducerLogic implements ProducerTaskService {
    @Autowired
    private ProducerStore producerStore;

    @Override
    public String registerProducer(ProducerCdo producerCdo) {
        Producer producer = new Producer();
        producerStore.create(producer);
        return producer.getId();
    }

    @Override
    public Producer findProducer(String producerId) {
        Producer producer = producerStore.retrieve(producerId);
        if (producer == null) {
            throw new NoSuchElementException("Producer id: " + producerId);
        }
        return producer;
    }

    @Override
    public List<Producer> findAllProducers() {
        List<Producer> producers = producerStore.retrieveAll(Offset.newOne(0, 100));
        if (producers.isEmpty()) {
            throw new NoSuchElementException("Producer");
        }
        return producers;
    }

    @Override
    public void modifyProducer(String producerId, NameValueList nameValues) {
        Producer producer = producerStore.retrieve(producerId);
        producer.setValues(nameValues);
        producerStore.update(producer);
    }

    @Override
    public void removeProducer(String producerId) {
        Producer producer = producerStore.retrieve(producerId);
        producerStore.delete(producer);
    }

    @Override
    public long countAllProducer() {
        return producerStore.countAll();
    }
}
