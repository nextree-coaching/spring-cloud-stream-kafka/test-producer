package io.naradrama.producer.aggregate.producer.service;

import io.naradrama.producer.aggregate.producer.entity.Producer;
import io.naradrama.producer.facade.aggregate.producer.sdo.ProducerCdo;
import io.naradrama.shared.NameValueList;

import java.util.List;

public interface ProducerTaskService {
    String registerProducer(ProducerCdo producerCdo);
    Producer findProducer(String producerId);
    List<Producer> findAllProducers();
    void modifyProducer(String producerId, NameValueList nameValues);
    void removeProducer(String producerId);
    long countAllProducer();
}
