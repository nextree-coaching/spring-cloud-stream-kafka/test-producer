package io.naradrama.producer.aggregate.producer.store.jpastore.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import io.naradrama.producer.aggregate.producer.store.jpastore.jpo.ProducerJpo;

public interface ProducerRepository extends PagingAndSortingRepository<ProducerJpo, String> {
}
