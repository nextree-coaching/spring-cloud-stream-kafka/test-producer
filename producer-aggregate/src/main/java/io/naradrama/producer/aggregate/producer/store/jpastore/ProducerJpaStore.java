package io.naradrama.producer.aggregate.producer.store.jpastore;

import org.springframework.stereotype.Repository;
import io.naradrama.producer.aggregate.producer.store.ProducerStore;
import io.naradrama.producer.aggregate.producer.store.jpastore.repository.ProducerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import io.naradrama.producer.aggregate.producer.entity.Producer;
import io.naradrama.producer.aggregate.producer.store.jpastore.jpo.ProducerJpo;
import java.util.Optional;
import java.util.List;
import io.naradrama.shared.Offset;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import java.util.stream.Collectors;

@Repository
public class ProducerJpaStore implements ProducerStore {
    @Autowired
    private ProducerRepository producerRepository;

    @Override
    public void create(Producer producer) {
        producerRepository.save(new ProducerJpo(producer));
    }

    @Override
    public Producer retrieve(String id) {
        Optional<ProducerJpo> optionalProducerJpo = producerRepository.findById(id);
        return optionalProducerJpo.map(ProducerJpo::toDomain).orElse(null);
    }

    @Override
    public List<Producer> retrieveAll(Offset offset) {
        Pageable pageable = PageRequest.of(offset.page(), offset.limit(), Sort.Direction.ASC, "name");
        Page<ProducerJpo> producerJpoPage = producerRepository.findAll(pageable);
        return producerJpoPage.stream().map(ProducerJpo::toDomain).collect(Collectors.toList());
    }

    @Override
    public void update(Producer producer) {
        producerRepository.save(new ProducerJpo(producer));
    }

    @Override
    public void delete(Producer producer) {
        producerRepository.deleteById(producer.getId());
    }

    @Override
    public boolean exists(String id) {
        return producerRepository.existsById(id);
    }

    @Override
    public long countAll() {
        return producerRepository.count();
    }
}
