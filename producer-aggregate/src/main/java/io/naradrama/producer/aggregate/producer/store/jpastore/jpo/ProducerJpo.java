package io.naradrama.producer.aggregate.producer.store.jpastore.jpo;

import io.naradrama.shared.JsonSerializable;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import io.naradrama.producer.aggregate.producer.entity.Producer;
import org.springframework.beans.BeanUtils;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "PRODUCER")
public class ProducerJpo implements JsonSerializable {
    @Id
    private String id;
    private String name;
    private int sequence;
    private String email;

    public ProducerJpo(Producer producer) {
        BeanUtils.copyProperties(producer, this);
    }

    public Producer toDomain() {
        Producer producer = new Producer(getId());
        BeanUtils.copyProperties(this, producer);
        return producer;
    }

    public static List<Producer> toDomains(List<ProducerJpo> producerJpos) {
        return producerJpos.stream().map(ProducerJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        return toJson();
    }

    public static ProducerJpo sample() {
        return new ProducerJpo(Producer.sample());
    }

    public static void main(String[] args) {
        System.out.println(sample());
    }
}
