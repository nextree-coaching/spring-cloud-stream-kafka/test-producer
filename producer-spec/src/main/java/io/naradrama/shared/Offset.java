package io.naradrama.shared;

import io.naradrama.shared.JsonSerializable;
import io.naradrama.shared.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Offset implements JsonSerializable {
    //
    private int offset;
    private int limit;
    private long totalCount;
    private boolean totalCountRequested;
    private String sortingField;
    private SortDirection sortDirection;

    public Offset(int offset,
                  int limit) {
        //
        this.offset = offset;
        this.limit = limit > 0 ? limit : 10;
        this.sortDirection = SortDirection.ASCENDING;
        this.totalCountRequested = false;
    }

    public Offset(int offset,
                  int limit,
                  SortDirection sortDirection,
                  String sortingField) {
        //
        this.offset = offset;
        this.limit = limit > 0 ? limit : 10;
        this.sortDirection = sortDirection;
        this.sortingField = sortingField;
        this.totalCountRequested = false;
    }

    public static Offset newDefault() {
        //
        return new Offset(0, 10);
    }

    public static Offset newOne(int offset,int limit) {
        //
        return new Offset(offset, limit);
    }

    public static Offset newOne(int offset,int limit, SortDirection sortDirection, String sortingField) {
        //
        return new Offset(offset, limit, sortDirection, sortingField);
    }

    public boolean ascendingSort() {
        //
        return SortDirection.ASCENDING == this.sortDirection;
    }

    public String toString() {
        //
        return toJson();
    }

    public static Offset fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Offset.class);
    }

    public static Offset sample() {
        //
        return new Offset(0,20);
    }

    public int offset() {
        //
        return offset;
    }

    public int limit() {
        //
        return limit;
    }

    public int page() {
        //
        return (offset/limit);
    }

    public int sum() {
        //
        return offset + limit;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
        System.out.println(sample().ascendingSort());
    }

    public enum SortDirection {
        //
        ASCENDING,
        DESCENDING
    }
}
