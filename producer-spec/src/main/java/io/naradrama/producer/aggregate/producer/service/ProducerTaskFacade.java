package io.naradrama.producer.aggregate.producer.service;

import io.naradrama.producer.aggregate.producer.entity.Producer;
import java.util.List;
import io.naradrama.shared.NameValueList;

public interface ProducerTaskFacade {
    Producer findProducer(String producerId);
    List<Producer> findAllProducers();
    void modifyProducer(String producerId, NameValueList nameValues);
    void removeProducer(String producerId);
    long countAllProducer();
}
