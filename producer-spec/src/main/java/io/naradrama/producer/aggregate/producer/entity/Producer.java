package io.naradrama.producer.aggregate.producer.entity;

import io.naradrama.shared.JsonSerializable;
import io.naradrama.shared.JsonUtil;
import io.naradrama.shared.NameValueList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Producer implements JsonSerializable {
    //
    private String id;
    private String name;
    private int sequence;
    private String email;

    public Producer(String id) {
        //
        this.id = id;
    }

    public String toString() {
        return toJson();
    }

    public static Producer fromJson(String json) {
        return JsonUtil.fromJson(json, Producer.class);
    }

    public void setValues(NameValueList nameValues) {
    }

    public static Producer sample() {
        //
        Producer sample = new Producer("R2@NP1-M1");
        sample.name = "test event";
        sample.sequence = 100;
        sample.email = "yspark@nextree.io";
        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
