package io.naradrama.producer.aggregate.producer.event;

import io.naradrama.producer.aggregate.producer.entity.Producer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TestEvent {
    //
    private Producer producer;

    public TestEvent(Producer producer) {
        this.producer = producer;
    }
}
