package io.naradrama.producer.facade.aggregate.producer.sdo;

import io.naradrama.shared.JsonSerializable;
import io.naradrama.shared.JsonUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import io.naradrama.shared.JsonSerializable;
import io.naradrama.shared.JsonUtil;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProducerCdo implements JsonSerializable {
    private String id;
    private String name;

    public String toString() {
        return toJson();
    }

    public static ProducerCdo fromJson(String json) {
        return JsonUtil.fromJson(json, ProducerCdo.class);
    }
}
