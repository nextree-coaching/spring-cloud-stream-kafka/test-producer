package io.naradrama.producer.client.aggregate.producer;

import org.springframework.stereotype.Component;
import io.naradrama.producer.aggregate.producer.service.ProducerTaskFacade;
import org.springframework.web.reactive.function.client.WebClient;
import io.naradrama.producer.aggregate.producer.entity.Producer;
import java.util.List;
import io.naradrama.shared.NameValueList;

@Component
public class ProducerClient implements ProducerTaskFacade {
    private static final String url = "/producer/producers";
    private WebClient webClient;

    public ProducerClient(WebClient webClient) {
        this.webClient = webClient;
    }

    @Override
    public Producer findProducer(String producerId) {
        return null;
    }

    @Override
    public List<Producer> findAllProducers() {
        return null;
    }

    @Override
    public void modifyProducer(String producerId, NameValueList nameValues) {
        return;
    }

    @Override
    public void removeProducer(String producerId) {
        return;
    }

    @Override
    public long countAllProducer() {
        return 0l;
    }
}
