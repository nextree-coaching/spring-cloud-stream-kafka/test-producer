package io.naradrama.producer.rest.aggregate.producer;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naradrama.producer.aggregate.producer.service.ProducerTaskFacade;
import io.naradrama.producer.aggregate.producer.entity.Producer;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;
import io.naradrama.shared.NameValueList;

@RestController
@RequestMapping("/producer/producers")
public class ProducerResource implements ProducerTaskFacade {

    @Override
    @GetMapping("{producerId}")
    public Producer findProducer(String producerId) {
        return null;
    }

    @Override
    @GetMapping
    public List<Producer> findAllProducers() {
        return null;
    }

    @Override
    public void modifyProducer(String producerId, NameValueList nameValues) {
    }

    @Override
    public void removeProducer(String producerId) {
    }

    @Override
    public long countAllProducer() {
        return 0l;
    }

}
