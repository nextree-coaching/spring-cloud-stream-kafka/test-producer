package io.naradrama.producer.rest.aggregate.producer;

//import io.naradrama.consumer.client.aggregate.consumer.ConsumerClient;
import io.naradrama.producer.aggregate.producer.entity.Producer;
import io.naradrama.producer.aggregate.eventproxy.kafka.TestEventBinder;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("test")
public class TestResource {
    //
    private final TestEventBinder eventBinder;
//    ConsumerClient consumerClient;


    @GetMapping("produce")
    public void porduce(){
        //
        Producer producer = Producer.sample();
        eventBinder.publishTestEvent(producer);
    }

    @GetMapping("test")
    public String test(){
        //
//        return consumerClient.test();
        return null;
    }
}
