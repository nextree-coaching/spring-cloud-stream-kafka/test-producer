package io.naradrama.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(
        scanBasePackages={
                "io.naradrama.producer",
                "io.naradrama.consumer",
        }
)
@EnableSwagger2
@EnableDiscoveryClient
public class ProducerNetflixBootApplication {
    //
    public static void  main(String[] args){
        //
        SpringApplication.run(ProducerNetflixBootApplication.class, args);
    }
}